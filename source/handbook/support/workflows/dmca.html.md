---
layout: markdown_page
title: DMCA Removal Requests
category: Legal
---

### On this page
{:.no_toc}

- TOC
{:toc}

----

As of March 2019, the Abuse Team has taken ownership of [DMCA requests](/handbook/engineering/security/dmca-removal-requests.html).
